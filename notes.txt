Create an empty project that we will host on our gitlab - Done
Integrate Animate CSS on or Project - Done
use Coolors/Unsplash as resources - Done
	https://coolors.co/ , https://unsplash.com/

CDN - Done
CSS unit measure - Done
CSP 1 code along template - Done

Stretch goal: Media queries


Reminders before hosting your project:
1. Make sure that CDNs are declared, not your local file for your bootstrap, jquery and popper
2. Create a .gitignore file, then add the name of the folders/files you don't want to be pushed on your git repo
3. Create a yml file for your project


CSS Unit Measure
px - pixels of the device, 1px is equivalent to one device pixel (dot) of the display. 1px = 1/96th of 1 in
	- fonts, low resolution images
em - relative to the font-size of the element
	(fontsize - 16px -> fontsize - 2em, means 2 times the size of the current font)
	parent element - 20px 
	child element - 3em -> 60px
rem - relative to the font-size of the root element
	(default browser's font size is - 16px -> font size = 5rem)
	p - font size - 2rem - 32px
	span - 3rem - 48px
	div - 4rem -64px
	for padding/margin, they are also based on the browser's font-size (16px)
vw - Relative to the 1% of the width of the viewport
	browser's size 100cm wide, 1 vw = 1cm
vh - Relative to the 1% of the height of the viewport
% - Relative to the parent element


Media queries